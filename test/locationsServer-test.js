var Server = require("../src/locationsServer");
var chai = require('chai');
var assert = require('chai').assert;
var chaiHttp = require("chai-http");
var Constants = require("../src/constants");

chai.use(chaiHttp);

describe("hapi server tests", function () {
  var server;
  const dbName = "unitTests";
  const schemaName = "hapiServer";

  before(function () {
    server = new Server(dbName, schemaName);
  });
  
  after(function () {
    return server.stop();
  });

  afterEach(function () {
    return server.clearDB();
  });

  describe("create endpoint", function () {

    it("should return created resource", function (done) {
      var input = { userId: 44, lat: 33, lng: 244 };

      chai.request(Constants.server)
      .post(Constants.endpoint)
      .send(input)
      .end(function (error, response) {
        assert.equal(response.status, Constants.created);
        var result = response.body;
        assert.equal(result.userId, input.userId);
        assert.equal(result.lat, input.lat);
        assert.equal(result.lng, input.lng);
        done();
      });
    });

    it("should return 400 error on bad request", function (done) {
      var input = { userId: 44 };

      chai.request(Constants.server)
      .post(Constants.endpoint)
      .send(input)
      .end(function (error, response) {
        assert.equal(response.status, Constants.badRequest);
        done();
      });
    });
  });

  describe("list endpoint", function () {

    it("should test list endpoint", function (done) {
      var input = { userId: 44, lat: 33, lng: 244 };

      chai.request(Constants.server)
      .post(Constants.endpoint)
      .send(input)
      .end();
      
      chai.request(Constants.server)
      .get(Constants.endpoint)
      .end(function (error, response) {
        assert.equal(response.status, Constants.ok);
        var result = response.body[0];
        assert.equal(result.userId, input.userId);
        assert.equal(result.lat, input.lat);
        assert.equal(result.lng, input.lng);

        done();
      });
    });
  });

  describe("get endpoint", function () {

    it("should return empty result if not existing", function (done) {
      chai.request(Constants.server)
      .get(Constants.endpoint + "/999")
      .end(function (error, response) {
        assert.equal(response.status, Constants.notFound);
        assert.empty(response.body);
        done();
      });
    });

    it("should test get endpoint", function (done) {
      var input = { userId: 44, lat: 33, lng: 244 };

      chai.request(Constants.server)
      .post(Constants.endpoint)
      .send(input)
      .end(function () {
        chai.request(Constants.server)
        .get(Constants.endpoint + "/" + input.userId)
        .end(function (error, response) {
          assert.equal(response.status, Constants.ok);
          var result = response.body;
          assert.equal(result.userId, input.userId);
          assert.equal(result.lat, input.lat);
          assert.equal(result.lng, input.lng);
          done();
        });
      });
    });
  });

  describe("update endpoint", function () {

    it("should update existing resource", function (done) {
      var input = { userId: 44, lat: 33, lng: 244 };
      var updated = {lat: 66, lng: 109};

      chai.request(Constants.server)
      .post(Constants.endpoint)
      .send(input)
      .end(function () {
        chai.request(Constants.server)
        .put(Constants.endpoint + "/" + input.userId)
        .send(updated)
        .end(function (error, response) {
          assert.equal(response.status, Constants.ok);
          var result = response.body;
          assert.equal(result.userId, input.userId);
          assert.equal(result.lat, updated.lat);
          assert.equal(result.lng, updated.lng);
          done();
        });
      });
    });

    it("should return bad request for invalid params", function (done) {
      chai.request(Constants.server)
      .put(Constants.endpoint + "/33")
      .send({lat: 44})
      .end(function (error, response) {
        assert.equal(response.status, Constants.badRequest);
        done();
      });
    });

    it("should return not found if the case", function (done) {
      chai.request(Constants.server)
      .put(Constants.endpoint + "/33")
      .send({lat: 44, lng: 55})
      .end(function (error, response) {
        assert.equal(response.status, Constants.notFound);
        done();
      });
    });
  });

  describe("delete endpoint", function () {

    it("should delete existing resource", function (done) {
      var input = { userId: 44, lat: 33, lng: 244 };

      chai.request(Constants.server)
      .post(Constants.endpoint)
      .send(input)
      .end(function () {
        chai.request(Constants.server)
        .delete(Constants.endpoint + "/" + input.userId)
        .end(function (error, response) {
          assert.equal(response.status, Constants.noContent);
          done();
        });
      });
    });

    it("should return resource not found if non-existing", function (done) {
      chai.request(Constants.server)
      .delete(Constants.endpoint + "/999")
      .end(function (error, response) {
        assert.equal(response.status, Constants.notFound);
        done();
      });
    });
  });
});