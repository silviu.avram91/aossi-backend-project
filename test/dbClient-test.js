var dbClient = require("../src/dbClient");
var assert = require("chai").assert;
var Promise = require("bluebird");

describe("mongoDB server tests", function () {
  const dbName = "unitTests";
  const schemaName = "mongoDBserver";
  var testDBClient;

  before(function () {
    testDBClient = new dbClient(dbName, schemaName);
  });

  after(function () {
    return testDBClient.close();
  });

  afterEach(function () {
    return testDBClient.clear();
  });

  describe("for create", function () {

    it("should create item", function () {
      var [userId, lat, lng] = [123, 45, 66];

      return testDBClient.create(userId, lat, lng)
      .then(function (result) {
        assert.equal(result.userId, userId);
        assert.equal(result.lat, lat);
        assert.equal(result.lng, lng);
      });
    });

    it("should create two items one after another", function () {
      var inputArrays = [[123, 45, 66], [43, 21, 33]];

      return Promise.mapSeries(inputArrays, function (inputArray, index) {
        var [userId, lat, lng] = inputArray;

        return testDBClient.create(userId, lat, lng)
        .then(function (result) {
          assert.equal(result.userId, userId);
          assert.equal(result.lat, lat);
          assert.equal(result.lng, lng);
        });
      });
    });

    it("should not create duplicated element", function () {
      var [userId, lat, lng] = [123, 45, 66];

      return testDBClient.create(userId, lat, lng)
      .then(() => testDBClient.create(userId, lat, lng))
      .catch(function (error) {
        assert.equal(error.message, "E11000 duplicate key error collection: " + dbName + "." + schemaName.toLowerCase() + "s index: userId_1 dup key: { : " + userId + " }");
      });
    });
  });

  describe("for list", function () {

    it("should return empty list", function () {
      return testDBClient.list().then(function (result) {
        assert.isEmpty(result);
      });
    });

    it("should not return empty list", function () {
      var [userId, lat, lng] = [123, 45, 66];

      return testDBClient.create(userId, lat, lng)
      .then(() => testDBClient.list())
      .then(function (results) {
        assert.isNotEmpty(results);
      });
    });

    it("should list two inserted elements", function () {
      var inputArrays = [[123, 45, 66], [43, 21, 33]];

      return Promise.mapSeries(inputArrays, function (inputArray, index) {
        var [userId, lat, lng] = inputArray;

        return testDBClient.create(userId, lat, lng)
        .then(() => testDBClient.list())
        .then(function (results) {
          var result = results[index];
          assert.isNotEmpty(results);
          assert.equal(results.length, index + 1);
          assert.equal(result.userId, userId);
          assert.equal(result.lat, lat);
          assert.equal(result.lng, lng);
        });
      });
    });
  });

  describe("for get", function () {

    it("should return null for non-existing element", function () {
      return testDBClient.get(123)
      .then(function (result) {
        assert.isNull(result);
      });
    });

    it("should return the inserted element", function () {
      var [userId, lat, lng] = [123, 45, 66];

      return testDBClient.create(userId, lat, lng)
      .then(() => testDBClient.get(userId))
      .then(function (result) {
        assert.equal(result.userId, userId);
        assert.equal(result.lat, lat);
        assert.equal(result.lng, lng);
      });
    });

    it("should return the inserted multiple elements", function () {
      var inputArrays = [[123, 45, 66], [43, 21, 33]];

      return Promise.mapSeries(inputArrays, function (inputArray) {
        var [userId, lat, lng] = inputArray;

        return testDBClient.create(userId, lat, lng);
      }).then(function (result) {
        return Promise.mapSeries(inputArrays, function (inputArray) {
          var [userId, lat, lng] = inputArray;

          return testDBClient.get(userId)
          .then(function (result) {
            assert.equal(result.userId, userId);
            assert.equal(result.lat, lat);
            assert.equal(result.lng, lng);
          });
        });
      });
    });
  });

  describe("for update", function () {

    it("should update element", function () {
      var [userId, lat, lng] = [123, 45, 66];
      var newLat = 99;
      var newLng = 140;

      return testDBClient.create(userId, lat, lng)
      .then(() => testDBClient.update(userId, newLat, newLng))
      .then(function (result) {
        assert.equal(result.userId, userId);
        assert.equal(result.lat, newLat);
        assert.equal(result.lng, newLng);
      });
    });

    it("should not update element if not found", function () {
      var [userId, lat, lng] = [123, 45, 66];

      return testDBClient.create(userId, lat, lng)
      .then(() => testDBClient.update(44, 11, 22))
      .then(function (result) {
        assert.isNull(result);
      });
    });

    it("should update element twice", function () {
      var [userId, lat, lng] = [123, 45, 66];
      var newLat = 99;
      var newLng = 140;

      return testDBClient.create(userId, lat, lng)
      .then(() => testDBClient.update(userId, newLat, newLng))
      .then(function (result) {
        assert.equal(result.userId, userId);
        assert.equal(result.lat, newLat);
        assert.equal(result.lng, newLng);

        return testDBClient.update(userId, lat, lng);
      }).then(function (result) {
        assert.equal(result.userId, userId);
        assert.equal(result.lat, lat);
        assert.equal(result.lng, lng);
      });
    });
  });

  describe("for delete", function () {
    
    it("should not delete anything if no relevant data", function () {
      return testDBClient.delete(112)
      .then(function (result) {
        assert.isNull(result);
      });
    });

    it("should delete item", function () {
      var [userId, lat, lng] = [123, 45, 66];

      return testDBClient.create(userId, lat, lng)
      .then(() => testDBClient.delete(userId))
      .then(() => testDBClient.get(userId))
      .then(function (result) {
        assert.isNull(result);
      });
    });

    it("should delete updated element", function () {
      var [userId, lat, lng] = [123, 45, 66];
      var newLat = 99;
      var newLng = 140;

      return testDBClient.create(userId, lat, lng)
      .then(() => testDBClient.update(userId, newLat, newLng))
      .then(() => testDBClient.delete(userId))
      .then(() => testDBClient.get(userId))
      .then(function (result) {
        assert.isNull(result);
      });
    });
  });

});