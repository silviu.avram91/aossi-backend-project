var mongoose = require("mongoose");
var Promise = require("bluebird");
var Constants = require("../src/constants");

mongoose.Promise = Promise;

var dbConnection = function (dbName, schemaName) {
  let connectionUrl = "mongodb://localhost/" + (dbName || Constants.dbDefaultName);
  mongoose.connect(connectionUrl);

  mongoose.connection.on('error', function (err) {
      console.log(err);
  });

  this.LocationsModel = mongoose.model(schemaName || Constants.schemaDefaultName, new mongoose.Schema({
    userId: {
      type: Number,
      unique: true
    },
    lat: Number,
    lng: Number
  }));
};

dbConnection.prototype.list = function (callback) {
  var locations = this.LocationsModel.find("{}", callback);
  console.log("HEHE");
  if (!callback) {
    return locations.exec(function (error, results) {
      return results;
    });
  }
};

dbConnection.prototype.get = function (userId, callback) {
  var locationModel = this.LocationsModel.findOne({userId: userId}, callback);

  if (!callback) {
    return locationModel.exec(function (error, result) {
      return result;
    });
  }
};

dbConnection.prototype.create = function (userId, lat, lng, callback) {
  var newLocationModel = new this.LocationsModel({
    userId: userId,
    lat: lat,
    lng: lng
  });

  return newLocationModel.save(callback || function (error, newLocation) {
    if (error) {
      throw error;
    } else {
      return newLocation;
    }
  });
}

dbConnection.prototype.update = function (userId, lat, lng, callback) {
  var updatedLocation = {
    lat: lat,
    lng: lng
  };

  var locationModel = this.LocationsModel.findOneAndUpdate({ userId: userId }, updatedLocation, {new: true}, callback);

  if (!callback) {
    return locationModel.exec(function (error, result) {
      if (error) {
        throw error;
      }
      return result;
    });
  }
};

dbConnection.prototype.delete = function (userId, callback) {
  var locationsModel = this.LocationsModel.findOneAndRemove({userId: userId}, callback);

  if (!callback) {
    return locationsModel.exec(function (error, result) {
      if (error) {
        throw error;
      }
      return result;
    });
  }
}

dbConnection.prototype.clear = function () {
  return this.LocationsModel.remove({});
};

dbConnection.prototype.close = function () {
  return mongoose.connection.close();
}

module.exports = dbConnection;
