var Hapi = require("hapi");
var Constants = require("../src/constants");
var DBClient = require("./dbClient");

var locationsServer = function (dbName, schemaName) {
  /* Initializations. */
  var server = new Hapi.Server();
  var dbClient = new DBClient(dbName, schemaName);
  server.connection({ port: 8080});

  /* Endpoints. */
  server.route({
    method: "GET",
    path: Constants.endpoint,
    handler: function (request, reply) {
      dbClient.list(function (error, results) {
        if (error) {
          reply().code(Constants.serverError);
        } else {
          reply(results).code(Constants.ok);
        }
      });
    }
  });

  server.route({
    method: "GET",
    path: Constants.endpoint + "/{userId}",
    handler: function (request, reply) {
      dbClient.get(request.params.userId, function (error, result) {
        if (error) {
          reply().code(Constants.serverError);
        } else {
          if (result == null) {
            reply().code(Constants.notFound);
          } else {
            reply(result).code(Constants.ok);
          }
        }
      });
    }
  });

  server.route({
    method: "POST",
    path: Constants.endpoint,
    handler: function (request, reply) {
      var content = request.payload;
      if (!content.userId || !content.lat || !content.lng) {
        reply().code(Constants.badRequest);
      } else {
        dbClient.create(content.userId, content.lat, content.lng, function (error, location) {
          reply(location).code(Constants.created);
        });
      }
    }
  });

  server.route({
    method: "PUT",
    path: Constants.endpoint + "/{userId}",
    handler: function (request, reply) {
      var content = request.payload;
      if (!content.lat || !content.lng) {
        reply().code(Constants.badRequest);
      } else {
        dbClient.update(request.params.userId, content.lat, content.lng, function (error, location) {
          if (location === null) {
            reply().code(Constants.notFound);
          } else {
            reply(location).code(Constants.ok);
          }
        });
      }
    }
  });

  server.route({
    method: "DELETE",
    path: Constants.endpoint + "/{userId}",
    handler: function (request, reply) {
      dbClient.delete(request.params.userId, function (error, location) {
        if (location === null) {
          reply().code(Constants.notFound);
        } else {
          reply().code(Constants.noContent);
        }
      });
    }
  });

  server.start(function (error) {});
  this.server = server;
  this.dbClient = dbClient;
};

locationsServer.prototype.stop = function () {
  return this.dbClient.close()
  .then(() => this.server.stop());
}

locationsServer.prototype.clearDB = function () {
  return this.dbClient.clear();
}

module.exports = locationsServer;
